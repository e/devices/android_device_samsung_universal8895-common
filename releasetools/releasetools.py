# Copyright (C) 2012 The Android Open Source Project
# Copyright (C) 2013-2016 The CyanogenMod Project
# Copyright (C) 2018 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import common
import re

def FullOTA_Assertions(info):
  # If we are shipping firmware make sure that we only flash on F variant
  if "RADIO/filemap" in info.input_zip.namelist():
    AddBlModelAssertion(info)

def FullOTA_InstallEnd(info):
  skip_firmware = False

  if "IMAGES/recovery.img" in info.input_zip.namelist():
    common.ZipWriteStr(info.output_zip, "firmware-update/recovery.img", info.input_zip.read("IMAGES/recovery.img"))
    info.script.AppendExtra('ui_print("Patching recovery image unconditionally...");')
    info.script.AppendExtra('package_extract_file("firmware-update/recovery.img", "/dev/block/platform/11120000.ufs/by-name/RECOVERY");')

  # Skip Firmware if filemap or any of the images are not present
  if "RADIO/filemap" not in info.input_zip.namelist():
    skip_firmware = True
  else:
    filemap = info.input_zip.read("RADIO/filemap").decode('utf-8').splitlines()
    for file in filemap:
      filename = file.split(" ")[0]
      if "RADIO/{}".format(filename) not in info.input_zip.namelist():
        skip_firmware = True
        break

  if not skip_firmware:
    CopyFirmware(info.input_zip, info.output_zip)
    AddFirmwareUpdate(info, filemap)

def CopyFirmware(input_zip, output_zip):
  for info in input_zip.infolist():
    f = info.filename
    # Copy files in 'RADIO' to output zip 'firmware-update'
    if f.startswith("RADIO/") and (f.__len__() > len("RADIO/")):
      fn = f[6:]
      common.ZipWriteStr(output_zip, "firmware-update/" + fn, input_zip.read(f))

def AddBlModelAssertion(info):
  info.script.AppendExtra('ifelse(universal8895.verify_bootloader_models() != "1",')
  info.script.AppendExtra('ui_print("=============================================");');
  info.script.AppendExtra('ui_print("=============================================");');
  info.script.AppendExtra('ui_print("                    ERROR:                   ");');
  info.script.AppendExtra('ui_print("          Package contains firmware!         ");');
  info.script.AppendExtra('ui_print("          Only the following models          ");');
  info.script.AppendExtra('ui_print("             are supported: F, FD            ");');
  info.script.AppendExtra('ui_print("          Cross flashing will brick!         ");');
  info.script.AppendExtra('ui_print("=============================================");');
  info.script.AppendExtra('ui_print("============NO CHANGE HAS BEEN MADE==========");');
  info.script.AppendExtra('abort(" ");')
  info.script.AppendExtra(');')

def CheckVariant(info, filemap, target_variant):
  # check if firmware for the variant is present
  if "RADIO/" + target_variant + "_sboot.bin" in info.input_zip.namelist():
   info.script.AppendExtra('ifelse(universal8895.compare_variant("' + target_variant + '") == "1",')
   info.script.AppendExtra('(')
   for file in filemap:
     filename = file.split(" ")[0]
     filepath = file.split(" ")[-1]
     filevariant = filename.split("_")[0]
     if filevariant == target_variant:
       info.script.AppendExtra('package_extract_file("firmware-update/' + filename + '", "' + filepath + '");')
   info.script.AppendExtra(')')
   info.script.AppendExtra(');')

def AddFirmwareUpdate(info, filemap):
  info.script.AppendExtra('ifelse(universal8895.verify_bootloader_min() != "1",')
  info.script.AppendExtra('(')
  info.script.AppendExtra('ui_print("Upgrading firmware");')

  # supported models are the same for s8 and s8 plus
  # F and FD are the same
  CheckVariant(info, filemap, "F")

  info.script.AppendExtra('run_program("/sbin/reboot","fota_bl");')
  info.script.AppendExtra('),')
  info.script.AppendExtra('(')
  info.script.AppendExtra('ui_print("Firmware is up-to-date");')
  info.script.AppendExtra(')')
  info.script.AppendExtra(');')