/*
 * Copyright (C) 2021, The LineageOS Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <android-base/properties.h>

#include "edify/expr.h"
#include "otautil/error_code.h"
Value *CompareVariant(const char *name, State *state,
                        const std::vector<std::unique_ptr<Expr>> &argv) {
    int ret = 0;
    std::string bootloader = android::base::GetProperty("ro.boot.bootloader", "");
    if (bootloader.empty()) {
        return ErrorAbort(state, kFileGetPropFailure,
                      "%s() failed to read current bootloader version", name);
    }

    std::string target_variant_arg;
    if (argv.empty() || !Evaluate(state, argv[0], &target_variant_arg)) {
        return ErrorAbort(state, kArgsParsingFailure,
                      "%s() error parsing arguments", name);
    }

    if (target_variant_arg.compare(bootloader.substr(4, target_variant_arg.length())) == 0) {
        ret = 1;
    }

    return StringValue(std::to_string(ret));
}

/**
 * for more informations
 * see https://android.stackexchange.com/questions/202491/what-do-the-numbers-and-letters-in-the-samsung-firmware-mean/202494#202494
 */
int compareBootloader(std::string a, std::string b) {
    /* compare bootloader version */
    if (std::stoi(a.substr(a.length() - 5), nullptr, 32) < std::stoi(b.substr(b.length() - 5), nullptr, 32)) {
        return 0;
    }

    // if version on the device is the same or newer dont upgrade
    return 1;
}


Value *VerifyBootloaderMin(const char *name, State *state,
                        const std::vector<std::unique_ptr<Expr>> &argv __unused) {
    int ret = 0;
    std::string required_bootloader_version;
    std::string bootloader = android::base::GetProperty("ro.boot.bootloader", "");
    if (bootloader.empty()) {
        return ErrorAbort(state, kFileGetPropFailure,
                      "%s() failed to read current bootloader version", name);
    }

    bool isDream2 = bootloader.substr(0, 4).compare("G955") == 0;

    if (bootloader.substr(4, 1).compare("F") == 0) { /* matches F and FD */
        ret = compareBootloader(bootloader, isDream2 ? "G955FXXUCDVI1" : "G950FXXUCDVI1");
    }

    return StringValue(std::to_string(ret));
}

Value *VerifyBootloaderModel(const char *name, State *state,
                        const std::vector<std::unique_ptr<Expr>> &argv __unused) {
    int ret = 0;
    std::string bootloader = android::base::GetProperty("ro.boot.bootloader", "");
    if (bootloader.empty()) {
        return ErrorAbort(state, kFileGetPropFailure,
                      "%s() failed to read current bootloader version", name);
    }

    if (bootloader.substr(4, 1).compare("F") == 0) { /* matches F and FD */
        ret = 1;
    }

    return StringValue(std::to_string(ret));
}

void Register_librecovery_updater_universal8895() {
  RegisterFunction("universal8895.compare_variant", CompareVariant);
  RegisterFunction("universal8895.verify_bootloader_min", VerifyBootloaderMin);
  RegisterFunction("universal8895.verify_bootloader_models", VerifyBootloaderModel);
}